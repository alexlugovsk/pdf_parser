# Generated by Django 4.0.5 on 2022-06-21 19:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sbornik',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pdf', models.FileField(upload_to='')),
                ('name', models.CharField(default='Новый сборник', max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Publications',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('keywords', models.CharField(blank=True, max_length=255, null=True)),
                ('authors', models.CharField(blank=True, max_length=255, null=True)),
                ('title', models.CharField(blank=True, default='Тема статьи', max_length=255, null=True)),
                ('text', models.TextField(default='Вся статья целиком')),
                ('sbornik', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.sbornik')),
            ],
        ),
    ]
