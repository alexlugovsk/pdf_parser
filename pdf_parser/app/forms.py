from django import forms
from app.models import Sbornik


class UploadFileForm(forms.ModelForm):
    class Meta:
        model = Sbornik
        fields = ['pdf', 'name']