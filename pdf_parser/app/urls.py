from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path("upload_success/", views.UploadSuccess, name='/upload_success/')
]
