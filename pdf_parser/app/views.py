from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import UploadFileForm
from .services import extract_text_from_pdf


def index(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            # file is saved
            object = form.save()
            extract_text_from_pdf.write_pdf_in_db(object)
            return HttpResponseRedirect('/upload_success/')
    else:
        form = UploadFileForm()
    return render(request, 'app/index.html', {'form': form})

def UploadSuccess(request):
    return render(request, 'app/upload_success.html')

