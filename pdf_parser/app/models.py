from django.db import models

# Create your models here.
class Sbornik(models.Model):
    pdf = models.FileField(upload_to='app/services/')
    name = models.CharField(max_length=255, default="Новый сборник")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Сборники'


class Publications(models.Model):
    sbornik = models.ForeignKey(Sbornik, on_delete=models.CASCADE)
    keywords = models.CharField(max_length=255, null=True, blank=True)
    authors = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(max_length=255, null=True, blank=True, default='Тема статьи')
    text = models.TextField(default='Вся статья целиком')
    ydk = models.CharField(default='', max_length=60)


    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Публикации'