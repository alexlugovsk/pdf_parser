from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.Publications)
class PublicationsAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'ydk',
        'sbornik',
        'authors',
        'keywords',
    )

    list_filter = (
        'sbornik',
        'ydk',
        'authors',
    )

    search_fields = (
        'id',
        'keywords',
        'authors',
        'title',
        'text',
        'ydk'
    )

@admin.register(models.Sbornik)
class SbornikAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'pdf'
    )

    search_fields = (
        'id',
        'name'
    )