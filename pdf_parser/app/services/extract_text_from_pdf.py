import io
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage
from app.models import Publications

def extract_text_from_pdf(pdf_path):
    resource_manager = PDFResourceManager()
    fake_file_handle = io.StringIO()
    converter = TextConverter(resource_manager, fake_file_handle)
    page_interpreter = PDFPageInterpreter(resource_manager, converter)

    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh,
                                      caching=True,
                                      check_extractable=True):
            page_interpreter.process_page(page)

        text = fake_file_handle.getvalue()

    # close open handles
    converter.close()
    fake_file_handle.close()

    if text:
        return text


def write_pdf_in_db(sbornik_obj):
    text = extract_text_from_pdf(sbornik_obj.pdf.name)
    split_text = text.split('УДК ') #разбиваем файл по УДК, тем самым получаем раздельно каждую статью.
    count_pub = 1 #счет специально не с 0, а с 1, так как в первой итерации данные не те
    while count_pub < len(split_text):
        #парсим УДК
        range_with_ydk = split_text[count_pub][0:600]
        ydk = ''
        count = 0
        while count < len(range_with_ydk):
            if len(ydk) < 1 and range_with_ydk[count] == ' ':
                count += 1
                continue
            elif range_with_ydk[count] != ' ':
                ydk += range_with_ydk[count]
                count += 1
                continue
            else:
                count +=1
                break
        print(f"ydk: {ydk}")


        #парсим авторов
        count2 = 0
        authors = ''
        authors_area = range_with_ydk[count:]
        while count2 < len(authors_area):

            if authors_area[count2] == ' ' and authors == '':
                count2 += 1
                continue

            elif authors_area[count2] != ' ' and authors == '':
                authors+=authors_area[count2]
                count2 += 1
                continue

            elif len(authors) > 0:
                if authors[-1] == ',' and authors_area[count2] == ' ':
                    count2 += 1
                    continue
                elif authors[-1] == '.' and authors_area[count2] == ' ':
                    count2 += 1
                    continue

                elif authors[-1] != ',' and authors_area[count2] == ' ':
                    break

                else:
                    authors+=authors_area[count2]
                    count2 += 1
                    continue
        print(f'authors: {authors}')


        #парсим тему статьи
        title_area = authors_area[count2:]
        city_start_index = title_area.find('г.')
        title_area_new = title_area[city_start_index+2:]
        count3 = 0
        title = ''
        print(f'title_area_new: {title_area_new}')
        while count3 < len(title_area_new):
            print(f'count3: {count3}')
            substr = title_area_new[count3]+title_area_new[count3+1]+title_area_new[count3+2]
            if title == '' and substr.isupper() == False:
                print('Первый if')
                count3 += 1
                continue
            elif substr.isupper() == True:
                print('Первый elif')
                count3 += 1
                title+=title_area_new[count3]
            elif substr.isupper() == False:
                print('Разрыв цикла')

                break


        print(f"title: {title}")


        #парсим ключевые слова
        keywords = ''
        keywords_start_index = split_text[count_pub].find('Ключевые слова: ')
        keywords_start_index = keywords_start_index + 16
        if (keywords_start_index != -1):
            keywords_end_index = split_text[count_pub].find('.', keywords_start_index)
            string_of_keywords = split_text[count_pub][keywords_start_index:keywords_end_index]
            string_of_keywords2 = string_of_keywords.replace(' ', '')
            #keywords = string_of_keywords2
            print(f"string_of_keywords2: {string_of_keywords2}")
        else:
            print('Не удалось найти ключевые слова')

        #заносим статью в БД
        pub = Publications()
        pub.sbornik = sbornik_obj
        pub.keywords = string_of_keywords2
        pub.text = split_text[count_pub]
        pub.authors = authors
        pub.title = title
        pub.ydk = ydk
        pub.save()

        count_pub+=1#сдвигаем счетчик для запуска алгоритма по следующей статье
